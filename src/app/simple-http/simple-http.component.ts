import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-simple-http',
  templateUrl: './simple-http.component.html',
  styleUrls: ['./simple-http.component.css']
})
export class SimpleHttpComponent implements OnInit {

  data: any;
  loading: boolean;

  constructor(private http: HttpClient) {

  }

  ngOnInit() {
  }

  makeRequest() {
    this.loading = true;
    this.http
      .get('https://jsonplaceholder.typicode.com/posts/1')
      .subscribe(data => {

        console.log('iaca data');
        console.log(data);

        this.data = data;
        this.loading = false;
      });
  }

}
